### Setup

#### Backend

1. Install Elixir 1.5 or higher(please update mix.exs if you are using 1.6 and
   above)
2. Install postgres and make sure there's `postgres` user with `postgres`
   password or update `config/dev.exs` with the root user information
2. Run `mix deps.get` to install dependencies
3. Run `mix phx.server` to start the server
4. Visit http://localhost:4100/graphiql to view Graphql playground and API
   documentations

#### Frontend

1. Install Nodejs, most of the versions should work
2. Run `yarn install` or `npm install` to install dependencies
3. Run `yarn build:tailwind` or `npm run build:tailwind` to build tailwind.css
4. Run `yarn start` or `npm run start` to start the dev server
5. Visit http://localhost:4101 to play with Pizza Master UI
