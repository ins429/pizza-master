defmodule PizzaMasterWeb.Schema do
  use Absinthe.Schema

  alias PizzaMasterWeb.Resolvers

  import_types(PizzaMasterWeb.Schema.Types)

  query do
    field :pizza, :pizza do
      arg(:id, non_null(:id))

      resolve(&Resolvers.Pizza.find/2)
    end

    field :pizzas, list_of(:pizza) do
      resolve(&Resolvers.Pizza.list/2)
    end

    field :toppings, list_of(:topping) do
      resolve(&Resolvers.Topping.list/2)
    end
  end

  mutation do
    @desc "create a topping"
    field :create_topping, :topping do
      arg(:name, :string)

      resolve(&Resolvers.Topping.create/2)
    end

    @desc "update a topping"
    field :update_topping, :topping do
      arg(:id, non_null(:id))
      arg(:name, :string)

      resolve(&Resolvers.Topping.update/2)
    end

    @desc "delete a topping"
    field :delete_topping, :topping do
      arg(:id, non_null(:id))

      resolve(&Resolvers.Topping.delete/2)
    end

    @desc "create a pizza"
    field :create_pizza, :pizza do
      arg(:name, :string)

      resolve(&Resolvers.Pizza.create/2)
    end

    @desc "update a pizza"
    field :update_pizza, :pizza do
      arg(:id, non_null(:id))
      arg(:name, :string)

      resolve(&Resolvers.Pizza.update/2)
    end

    @desc "delete a pizza"
    field :delete_pizza, :pizza do
      arg(:id, non_null(:id))

      resolve(&Resolvers.Pizza.delete/2)
    end

    @desc "add a topping to a pizza"
    field :add_topping, :pizza do
      arg(:pizza_id, non_null(:id))
      arg(:topping_id, non_null(:id))

      resolve(&Resolvers.Pizza.add_topping/2)
    end

    @desc "remove a topping to a pizza"
    field :remove_topping, :pizza do
      arg(:pizza_id, non_null(:id))
      arg(:topping_id, non_null(:id))

      resolve(&Resolvers.Pizza.remove_topping/2)
    end
  end
end
