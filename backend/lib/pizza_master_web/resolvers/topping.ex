defmodule PizzaMasterWeb.Resolvers.Topping do
  alias PizzaMaster.Pizzas
  alias PizzaMaster.Repo

  def list(_args, _) do
    Pizzas.list_toppings()
  end

  def create(%{name: name}, _) do
    Pizzas.create_topping(name)
  end

  def update(%{id: id, name: name}, _) do
    Pizzas.update_topping(id, name)
  end

  def delete(%{id: id}, _) do
    Pizzas.delete_topping(id)
  end

  def pizza_toppings(pizza, _, _) do
    {:ok,
     pizza
     |> Repo.preload(:toppings)
     |> Map.get(:toppings)}
  end
end
