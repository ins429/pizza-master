defmodule PizzaMasterWeb.Resolvers.Pizza do
  alias PizzaMaster.Pizzas

  def find(%{id: id}, _) do
    Pizzas.find_pizza(id)
  end

  def list(_args, _) do
    Pizzas.list_pizzas()
  end

  def create(%{name: name}, _) do
    Pizzas.create_pizza(name)
  end

  def update(%{id: id, name: name}, _) do
    Pizzas.update_pizza(id, name)
  end

  def add_topping(%{pizza_id: pizza_id, topping_id: topping_id}, _) do
    Pizzas.add_topping(pizza_id, topping_id)
  end

  def remove_topping(%{pizza_id: pizza_id, topping_id: topping_id}, _) do
    Pizzas.remove_topping(pizza_id, topping_id)
  end

  def delete(%{id: id}, _) do
    Pizzas.delete_pizza(id)
  end
end
