defmodule PizzaMasterWeb.Schema.Types do
  use Absinthe.Schema.Notation

  import Absinthe.Resolution.Helpers

  alias PizzaMasterWeb.Resolvers

  scalar :timestamp, name: "DateTime" do
    serialize(&serialize_timestamp/1)
    parse(&parse_timestamp/1)
  end

  object :topping do
    field(:id, :string)
    field(:name, :string)
    field(:created_at, :timestamp)
    field(:updated_at, :timestamp)
  end

  object :pizza do
    field(:id, :string)
    field(:name, :string)
    field(:toppings, list_of(:topping), resolve: &Resolvers.Topping.pizza_toppings/3)
    field(:created_at, :timestamp)
    field(:updated_at, :timestamp)
  end

  def serialize_timestamp(%DateTime{} = timestamp), do: DateTime.to_iso8601(timestamp)

  def parse_timestamp(%Absinthe.Blueprint.Input.String{value: value}) do
    case DateTime.from_iso8601(value) do
      {:ok, datetime, 0} -> {:ok, datetime}
      _error -> :error
    end
  end

  def parse_timestamp(%Absinthe.Blueprint.Input.Null{}) do
    {:ok, nil}
  end

  def parse_timestamp(_) do
    :error
  end
end
