defmodule PizzaMasterWeb.Router do
  use PizzaMasterWeb, :router

  scope "/" do
    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: PizzaMasterWeb.Schema,
      json_codec: Phoenix.json_library()

    forward "/graphql", Absinthe.Plug,
      schema: PizzaMasterWeb.Schema,
      json_codec: Phoenix.json_library()
  end
end
