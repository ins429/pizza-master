defmodule PizzaMaster.Pizzas.Topping do
  use PizzaMaster.Schema

  alias PizzaMaster.Pizzas.Pizza
  alias PizzaMaster.Pizzas.PizzaTopping

  schema "toppings" do
    field :name, :string

    timestamps()
  end

  def changeset(%__MODULE__{} = topping, attrs) do
    topping
    |> cast(attrs, [
      :name
    ])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end
end
