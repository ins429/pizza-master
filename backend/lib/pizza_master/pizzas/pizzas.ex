defmodule PizzaMaster.Pizzas do
  alias PizzaMaster.Repo

  alias PizzaMaster.Pizzas.{
    Pizza,
    Topping
  }

  def list_pizzas do
    {:ok, Pizza |> Repo.all()}
  end

  def find_pizza(id) do
    Repo.get(Pizza, id)
    |> case do
      %Pizza{} = pizza -> {:ok, pizza |> Repo.preload(:toppings)}
      nil -> {:error, :not_found}
    end
  end

  def create_pizza(name) do
    Pizza.changeset(%Pizza{}, %{name: name})
    |> Repo.insert()
  end

  def update_pizza(id, name) do
    with {:ok, pizza} <- find_pizza(id) do
      Pizza.changeset(pizza, %{name: name})
      |> Repo.update()
    end
  end

  def update_toppings(%Pizza{} = pizza, attrs) do
    Pizza.add_topping_changeset(pizza, attrs)
    |> Repo.update()
  end

  def add_topping(id, topping_id) do
    with {:ok, pizza} <- find_pizza(id),
         pizza <- Repo.preload(pizza, :pizza_toppings),
         topping_attrs <- build_toppings_attrs(pizza, topping_id),
         {:ok, pizza} <- update_toppings(pizza, topping_attrs),
         pizza <- Repo.preload(pizza, :toppings) do
      {:ok, pizza}
    end
  end

  def remove_topping(id, topping_id) do
    with {:ok, pizza} <- find_pizza(id),
         pizza <- Repo.preload(pizza, :pizza_toppings),
         topping_attrs <- build_toppings_attrs(pizza, topping_id, :remove),
         {:ok, pizza} <- update_toppings(pizza, topping_attrs),
         pizza <- Repo.preload(pizza, :toppings) do
      {:ok, pizza}
    end
  end

  def delete_pizza(id) do
    with {:ok, pizza} <- find_pizza(id) do
      pizza
      |> Repo.delete()
    end
  end

  def list_toppings do
    {:ok, Topping |> Repo.all()}
  end

  def find_topping(id) do
    Repo.get(Topping, id)
    |> case do
      %Topping{} = topping -> {:ok, topping}
      nil -> {:error, :not_found}
    end
  end

  def create_topping(name) do
    Topping.changeset(%Topping{}, %{name: name})
    |> Repo.insert()
  end

  def update_topping(id, name) do
    with {:ok, topping} <- find_topping(id) do
      Topping.changeset(topping, %{name: name})
      |> Repo.update()
    end
  end

  def delete_topping(id) do
    with {:ok, topping} <- find_topping(id) do
      topping
      |> Repo.delete()
    end
  end

  defp build_toppings_attrs(%Pizza{pizza_toppings: pizza_toppings}, topping_id) do
    %{
      pizza_toppings:
        pizza_toppings
        |> Enum.map(&Map.from_struct/1)
        |> Kernel.++([%{topping_id: topping_id}])
    }
  end

  defp build_toppings_attrs(pizza, "" <> topping_id, :remove) do
    build_toppings_attrs(pizza, String.to_integer(topping_id), :remove)
  end

  defp build_toppings_attrs(%Pizza{pizza_toppings: pizza_toppings}, topping_id, :remove) do
    %{
      pizza_toppings:
        pizza_toppings
        |> Enum.map(&Map.from_struct/1)
        |> Enum.filter(&(Map.get(&1, :topping_id) !== topping_id))
    }
  end
end
