defmodule PizzaMaster.Pizzas.PizzaTopping do
  use PizzaMaster.Schema

  alias PizzaMaster.Pizzas.Pizza
  alias PizzaMaster.Pizzas.Topping

  schema "pizzas_toppings" do
    belongs_to :pizza, Pizza
    belongs_to :topping, Topping

    timestamps()
  end

  def changeset(%__MODULE__{} = topping, attrs) do
    topping
    |> cast(attrs, [:pizza_id, :topping_id])
    |> assoc_constraint(:pizza)
    |> assoc_constraint(:topping)
  end
end
