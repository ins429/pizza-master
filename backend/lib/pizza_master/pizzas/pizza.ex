defmodule PizzaMaster.Pizzas.Pizza do
  use PizzaMaster.Schema

  alias PizzaMaster.Pizzas.PizzaTopping
  alias PizzaMaster.Pizzas.Topping

  schema "pizzas" do
    field :name, :string

    has_many :pizza_toppings, PizzaTopping, on_replace: :delete
    has_many :toppings, through: [:pizza_toppings, :topping]

    timestamps()
  end

  def changeset(%__MODULE__{} = pizza, attrs) do
    pizza
    |> cast(attrs, [
      :name
    ])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end

  def add_topping_changeset(%__MODULE__{} = pizza, attrs) do
    pizza
    |> cast(attrs, [])
    |> cast_assoc(:pizza_toppings)
  end
end
