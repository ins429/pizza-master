defmodule PizzaMaster.Schema do
  @moduledoc false

  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      import Ecto.Changeset

      @timestamps_opts type: :utc_datetime
    end
  end
end
