defmodule PizzaMaster.Repo do
  use Ecto.Repo,
    otp_app: :pizza_master,
    adapter: Ecto.Adapters.Postgres
end
