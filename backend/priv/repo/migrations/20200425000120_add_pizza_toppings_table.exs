defmodule PizzaMaster.Repo.Migrations.AddPizzaToppingsTable do
  use Ecto.Migration

  def change do
    create table(:pizzas_toppings) do
      add(:pizza_id, references(:pizzas, on_delete: :delete_all), null: false)
      add(:topping_id, references(:toppings, on_delete: :delete_all), null: false)

      timestamps()
    end
  end
end
