defmodule PizzaMaster.PizzasTest do
  use PizzaMaster.DataCase

  alias PizzaMaster.Repo
  alias PizzaMaster.Pizzas

  alias PizzaMaster.Pizzas.{
    Pizza,
    Topping
  }

  describe "add_topping/2" do
    test "adds a topping to a pizza with no toppings" do
      pizza = create_pizza()
      olive_topping = create_topping()

      assert {:ok, updated_pizza} = Pizzas.add_topping(pizza.id, olive_topping.id)
      assert has_topping?(updated_pizza, olive_topping)
    end

    test "adds a topping to a pizza with toppings" do
      olive_topping = create_topping()

      pizza =
        create_pizza(%{name: "olive mushroom"})
        |> Repo.preload(:pizza_toppings)
        |> Pizza.add_topping_changeset(%{
          pizza_toppings: [
            %{
              topping_id: olive_topping.id
            }
          ]
        })
        |> Repo.update!()

      mushroom_topping = create_topping(%{name: "mushroom"})

      assert {:ok, updated_pizza} = Pizzas.add_topping(pizza.id, mushroom_topping.id)
      assert has_topping?(updated_pizza, olive_topping)
      assert has_topping?(updated_pizza, mushroom_topping)
    end
  end

  describe "remove_topping/2" do
    test "removes a topping from a pizza" do
      olive_topping = create_topping()
      mushroom_topping = create_topping(%{name: "mushroom"})

      pizza =
        create_pizza(%{name: "olive mushroom"})
        |> Repo.preload(:pizza_toppings)
        |> Pizza.add_topping_changeset(%{
          pizza_toppings: [
            %{
              topping_id: olive_topping.id
            },
            %{
              topping_id: mushroom_topping.id
            }
          ]
        })
        |> Repo.update!()

      assert {:ok, updated_pizza} = Pizzas.remove_topping(pizza.id, olive_topping.id)
      assert has_topping?(updated_pizza, mushroom_topping)
      refute has_topping?(updated_pizza, olive_topping)
    end
  end

  defp create_pizza(attrs \\ %{name: "olive"}) do
    %Pizza{}
    |> Pizza.changeset(attrs)
    |> Repo.insert!()
  end

  defp create_topping(attrs \\ %{name: "olive"}) do
    %Topping{}
    |> Topping.changeset(attrs)
    |> Repo.insert!()
  end

  defp has_topping?(%{toppings: toppings}, topping) do
    toppings
    |> Enum.map(& &1.id)
    |> Enum.member?(topping.id)
  end
end
