import React from 'react'
import { useParams } from 'react-router-dom'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import ToppingSelector from 'components/ToppingSelector'
import useAddToppingMutation from 'hooks/mutations/useAddTopping'
import useRemoveToppingMutation from 'hooks/mutations/useRemoveTopping'
import { hasTopping } from 'utils'

const PIZZA_QUERY = gql`
  query PizzaQuery($id: String!) {
    pizza(id: $id) {
      id
      name
      toppings {
        id
        name
      }
    }
  }
`

const PizzaEditor = () => {
  const { pizzaId } = useParams()
  const { data, loading } = useQuery(PIZZA_QUERY, {
    variables: { id: pizzaId }
  })
  const [addTopping] = useAddToppingMutation()
  const [removeTopping] = useRemoveToppingMutation()

  if (loading || !data) {
    return 'loading...'
  }

  const { pizza } = data

  return (
    <div className="flex">
      <div className="flex flex-col mx-4">
        Toppings
        <ToppingSelector
          pizzaToppings={pizza.toppings}
          handleSelect={topping =>
            hasTopping(topping, pizza.toppings)
              ? removeTopping({ pizzaId: pizza.id, toppingId: topping.id })
              : addTopping({ pizzaId: pizza.id, toppingId: topping.id })
          }
        />
      </div>
    </div>
  )
}

export default PizzaEditor
