import React from 'react'
import { Link } from 'react-router-dom'

const sizeStyle = size => {
  switch (size) {
    case 'xs':
      return 'text-xs p-1'
    default:
      return 'py-2 px-4'
  }
}

const activeStyle = active =>
  active
    ? 'bg-blue-500 text-white border-blue-500'
    : 'hover:bg-blue-500 text-blue-700 hover:text-white border-blue-500 hover:border-transparent'

const baseStyle = 'bg-transparent border rounded'

const Button = ({ handleClick, className = '', active, size, ...props }) => (
  <button
    className={`${baseStyle} ${activeStyle(active)} ${sizeStyle(
      size
    )} ${className}`}
    {...props}
  />
)

export const ButtonLink = ({
  handleClick,
  className = '',
  active,
  size,
  ...props
}) => (
  <Link
    className={`${baseStyle} ${activeStyle(active)} ${sizeStyle(
      size
    )} ${className}`}
    {...props}
  />
)

export default Button
