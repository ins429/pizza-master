import React from 'react'
import { useParams } from 'react-router-dom'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import useDeletePizzaMutation from 'hooks/mutations/useDeletePizza'
import Button, { ButtonLink } from 'components/Button'

export const PIZZAS_QUERY = gql`
  query PizzasQuery {
    pizzas {
      id
      name
    }
  }
`

const PizzaSelector = () => {
  const { pizzaId } = useParams()
  const { data: { pizzas } = {}, loading } = useQuery(PIZZAS_QUERY, {
    fetchPolicy: 'network-only'
  })
  const [deletePizza] = useDeletePizzaMutation()

  if (loading) {
    return 'loading...'
  }

  return (
    <div className="flex flex-col">
      {pizzas &&
        pizzas.map(pizza => (
          <div key={pizza.id} className="mt-4">
            <ButtonLink
              active={pizza.id === pizzaId}
              key={pizza.id}
              to={`/pizzas/${pizza.id}`}
              className="mr-1"
            >
              {pizza.name}
            </ButtonLink>
            <Button
              size="xs"
              onClick={async () => {
                await deletePizza(pizza.id)
                window.location = '/pizzas'
              }}
            >
              Delete
            </Button>
          </div>
        ))}
    </div>
  )
}

export default PizzaSelector
