import React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { hasTopping } from 'utils'
import Button from 'components/Button'

const TOPPINGS_QUERY = gql`
  query ToppingsQuery {
    toppings {
      id
      name
    }
  }
`

const ToppingSelector = ({ pizzaToppings, handleSelect }) => {
  const { data, loading } = useQuery(TOPPINGS_QUERY)

  if (loading || !data) {
    return 'loading...'
  }

  const { toppings } = data

  return (
    <div className="flex flex-col">
      {toppings.map(topping => (
        <Button
          key={topping.id}
          className="mt-1"
          size="xs"
          active={hasTopping(topping, pizzaToppings)}
          handleClick={() => handleSelect(topping)}
        >
          {topping.name}
        </Button>
      ))}
    </div>
  )
}

export default ToppingSelector
