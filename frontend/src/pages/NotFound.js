import React from 'react'

const NotFound = () => (
  <div className="container h-screen flex items-center justify-center m-auto">
    <p className="text-xl">Page not found.</p>
  </div>
)

export default NotFound
