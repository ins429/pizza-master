import React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import useDeleteToppingMutation from 'hooks/mutations/useDeleteTopping'
import Button, { ButtonLink } from 'components/Button'

export const TOPPINGS_QUERY = gql`
  query ToppingsQuery {
    toppings {
      id
      name
    }
  }
`

const ToppingManager = () => {
  const { data, loading } = useQuery(TOPPINGS_QUERY, {
    fetchPolicy: 'network-only'
  })
  const [deleteTopping] = useDeleteToppingMutation()

  if (loading || !data) {
    return 'loading...'
  }

  const { toppings } = data

  return (
    <>
      <div className="mb-4">
        <ButtonLink to="/toppings/new" className="mr-2">
          Create a new topping
        </ButtonLink>
        <ButtonLink to="/">Back</ButtonLink>
      </div>
      <div className="flex flex-col mx-4 my-4">
        <h5 className="font-semibold text-lg">Toppings</h5>
        {toppings.map(topping => (
          <div className="mb-2" key={topping.id}>
            <span className="mr-1">{topping.name}</span>

            <Button
              size="xs"
              onClick={async () => {
                await deleteTopping(topping.id)
                window.location = '/toppings'
              }}
            >
              Delete
            </Button>
          </div>
        ))}
      </div>
    </>
  )
}

export default ToppingManager
