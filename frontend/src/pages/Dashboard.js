import React from 'react'
import { Link } from 'react-router-dom'

const Dashboard = () => (
  <>
    <Link
      className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded mr-2"
      to="/pizzas"
    >
      Manage Pizzas
    </Link>
    <Link
      className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"
      to="/toppings"
    >
      Manage Toppings
    </Link>
  </>
)

export default Dashboard
