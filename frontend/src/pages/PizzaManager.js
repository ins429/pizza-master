import React from 'react'
import { useParams } from 'react-router-dom'
import PizzaSelector from 'components/PizzaSelector'
import PizzaEditor from 'components/PizzaEditor'
import { ButtonLink } from 'components/Button'

const PizzaManager = () => {
  const { pizzaId } = useParams()

  return (
    <>
      <div className="mb-4">
        <ButtonLink to="/pizzas/new" className="text-center mr-2">
          Create a new pizza
        </ButtonLink>
        <ButtonLink to="/" className="text-center">
          Back
        </ButtonLink>
      </div>
      <div className="flex flex-row">
        <div className="mr-1">
          <PizzaSelector />
        </div>
        {pizzaId ? (
          <PizzaEditor />
        ) : (
          <div className="flex m-auto p-1">Please select a pizza</div>
        )}
      </div>
    </>
  )
}

export default PizzaManager
