import React, { useState } from 'react'
import { Link, Redirect } from 'react-router-dom'
import useCreatePizzaMutation from 'hooks/mutations/useCreatePizza'

const NewPizza = () => {
  const [name, setName] = useState('')
  const [createPizza, { data, loading }] = useCreatePizzaMutation()

  if (!loading && data) {
    return <Redirect to="/pizzas" />
  }

  return (
    <form
      className="bg-white shadow-xs w-11/12 max-w-xl rounded px-8 pt-6 pb-8 m-auto"
      onSubmit={e => {
        e.preventDefault()
        createPizza({ name })
      }}
    >
      <div className="mb-4">
        <h3 className="text-base">Create a new pizza</h3>
      </div>
      <div className="mb-4">
        <label
          className="block text-gray-700 text-xs font-bold mb-2"
          htmlFor="name"
        >
          Name
        </label>
        <input
          className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500"
          id="name"
          type="text"
          placeholder="Name"
          value={name}
          onChange={({ target: { value } }) => setName(value)}
        />
      </div>
      <div className="flex items-center justify-between">
        <button className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
          Create
        </button>
        <Link
          className="inline-block align-baseline font-bold text-xxs text-blue-500 hover:text-blue-800"
          to="/"
        >
          Cancel
        </Link>
      </div>
    </form>
  )
}

export default NewPizza
