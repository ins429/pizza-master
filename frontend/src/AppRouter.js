import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import NotFound from 'pages/NotFound'
import Dashboard from 'pages/Dashboard'
import NewPizza from 'pages/NewPizza'
import NewTopping from 'pages/NewTopping'
import PizzaManager from 'pages/PizzaManager'
import ToppingManager from 'pages/ToppingManager'

const AppRouter = () => (
  <Router>
    <Switch>
      <Route path="/" exact>
        <Dashboard />
      </Route>
      <Route path="/pizzas/new">
        <NewPizza />
      </Route>
      <Route path={['/pizzas', '/pizzas/:pizzaId']} exact>
        <PizzaManager />
      </Route>
      <Route path="/toppings" exact>
        <ToppingManager />
      </Route>
      <Route path="/toppings/new">
        <NewTopping />
      </Route>
      <Route path="*">
        <NotFound />
      </Route>
    </Switch>
  </Router>
)

export default AppRouter
