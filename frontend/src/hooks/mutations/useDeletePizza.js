import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

const DELETE_PIZZA = gql`
  mutation DeletePizza($id: String!) {
    deletePizza(id: $id) {
      id
      name
    }
  }
`

const useDeletePizzaMutation = options => {
  const [mutate, result] = useMutation(DELETE_PIZZA, options)

  const deletePizza = id =>
    mutate({
      variables: {
        id
      }
    })

  return [deletePizza, result]
}

export default useDeletePizzaMutation
