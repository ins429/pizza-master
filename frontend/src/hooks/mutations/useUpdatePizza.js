import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

const CREATE_PIZZA = gql`
  mutation CreatePizza($name: String!) {
    createPizza(name: $name) {
      id
      name
    }
  }
`

const useCreatePizzaMutation = options => {
  const [mutate, result] = useMutation(CREATE_PIZZA, options)

  const createPizza = ({ name }) =>
    mutate({
      variables: {
        name
      }
    })

  return [createPizza, result]
}

export default useCreatePizzaMutation
