import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

const CREATE_TOPPING = gql`
  mutation CreateTopping($name: String!) {
    createTopping(name: $name) {
      id
      name
    }
  }
`

const useCreateToppingMutation = options => {
  const [mutate, result] = useMutation(CREATE_TOPPING, options)

  const createTopping = ({ name }) =>
    mutate({
      variables: {
        name
      }
    })

  return [createTopping, result]
}

export default useCreateToppingMutation
