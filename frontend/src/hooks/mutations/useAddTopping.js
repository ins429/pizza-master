import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

export const ADD_TOPPING = gql`
  mutation AddTopping($pizzaId: String!, $toppingId: String!) {
    addTopping(pizzaId: $pizzaId, toppingId: $toppingId) {
      id
      name
      toppings {
        id
        name
      }
    }
  }
`

const useAddToppingMutation = options => {
  const [mutate, result] = useMutation(ADD_TOPPING, options)

  const addTopping = ({ pizzaId, toppingId }) =>
    mutate({
      variables: {
        pizzaId,
        toppingId
      }
    })

  return [addTopping, result]
}

export default useAddToppingMutation
