import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

export const REMOVE_TOPPING = gql`
  mutation RemoveTopping($pizzaId: String!, $toppingId: String!) {
    removeTopping(pizzaId: $pizzaId, toppingId: $toppingId) {
      id
      name
      toppings {
        id
        name
      }
    }
  }
`

const useRemoveToppingMutation = options => {
  const [mutate, result] = useMutation(REMOVE_TOPPING, options)

  const removeTopping = ({ pizzaId, toppingId }) =>
    mutate({
      variables: {
        pizzaId,
        toppingId
      }
    })

  return [removeTopping, result]
}

export default useRemoveToppingMutation
