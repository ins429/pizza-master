import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

const DELETE_TOPPING = gql`
  mutation DeleteTopping($id: String!) {
    deleteTopping(id: $id) {
      id
      name
    }
  }
`

const useDeleteToppingMutation = options => {
  const [mutate, result] = useMutation(DELETE_TOPPING, options)

  const deleteTopping = id =>
    mutate({
      variables: {
        id
      }
    })

  return [deleteTopping, result]
}

export default useDeleteToppingMutation
