export const hasTopping = (topping, toppings) =>
  Boolean(toppings.find(t => topping.id === t.id))
