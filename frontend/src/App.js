import React from 'react'
import { client } from 'config/apollo'
import { ApolloProvider } from '@apollo/react-hooks'
import Router from 'AppRouter'
import './App.css'

const App = () => {
  return (
    <div className="container flex flex-col my-4 mx-auto">
      <div className="mx-auto">
        <ApolloProvider client={client}>
          <Router />
        </ApolloProvider>
      </div>
    </div>
  )
}

export default App
